from pathlib import Path

from ruamel.yaml import YAML
from sopel.plugin import nickname_command


YAML_PARSER = YAML()
PLUGIN_FOLDER = Path(__file__).parent
QUEUE_PATH = PLUGIN_FOLDER / "dispenser-queue.yaml"
SENT_PATH = PLUGIN_FOLDER / "dispenser-sent.yaml"


def _get_po_url():
    queue = YAML_PARSER.load(QUEUE_PATH)
    sent = YAML_PARSER.load(SENT_PATH)
    file_url = queue.pop(0)
    sent.append(file_url)
    YAML_PARSER.dump(queue, QUEUE_PATH)
    YAML_PARSER.dump(sent, SENT_PATH)
    return file_url


@nickname_command("po")
def get_po_url(bot, trigger):
    bot.reply(_get_po_url())


@nickname_command("tbx")
def get_tbx_url(bot, trigger):
    bot.reply("http://termos.trasno.gal/completo.tbx (532,9 KiB!)")


@nickname_command("tmx")
def get_tmx_url(bot, trigger):
    bot.reply("https://gitlab.com/trasno/translation-memory/-/raw/main/kde.tmx?ref_type=heads&inline=false (36,6 MiB)")
